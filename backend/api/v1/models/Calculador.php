<?php

namespace backend\api\v1\models;

use Yii;
use yii\base\Model;


class Calculador extends Model{

	public $valor1;
	public $valor2;

	public function rules(){
		return[
			[['valor1','valor2'],'required'],
			[['valor1','valor2'],'number'],
		];
	}
}


?>