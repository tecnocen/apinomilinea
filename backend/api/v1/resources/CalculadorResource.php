<?php

namespace backend\api\v1\resources;

use tecnocen\roa\controllers\Resource;
use Yii;
use backend\api\v1\models\Calculador;

class CalculadorResource extends Resource{

	public $modelClass=Calculador::class;

	public function actions()
    {
        $actions = parent::actions();
        unset( $actions['create'],$actions['index'],$actions['delete']);
        return $actions;
    }

    public function actionCreate(){
    	$datos = ['Calculador'=>Yii::$app->request->post()];
    	$model= new $this->modelClass;
    	$model->load($datos);

    	if($model->validate()){
    		$suma = (float)$model->valor1+(float)$model->valor2;
    		Yii::$app->getResponse()->setStatusCode(201);
    		return 'La suma de los valores es:'.$suma;
    	}
    	return $model;
    }

}

?>