<?php
 
 
namespace backend\api\v1\behaviors;
 
use tecnocen\roa\behaviors\Slug;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\Link;
use yii\web\Linkable;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use backend\api\v1\models\Credito;
use yii\helpers\ArrayHelper;
use backend\api\v1\models\CreditoPagos;

 
class BehaviorCredito extends Behavior 
{
  
        public function events(){
            return [
                ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ];
        }

        public function afterInsert($event){
            $atributos = ArrayHelper::getValue($event, 'sender', []);
            //\yii\helpers\VarDumper::dump($atributos->convenio->plazos,10,true); die();
            $id = ArrayHelper::getValue($atributos, 'id', 0);
            $plazos = ArrayHelper::getValue($atributos, 'convenio.plazos', 0);
            $total_pagar = ArrayHelper::getValue($atributos, 'convenio.total_pagar', 0);
            $descuento = ArrayHelper::getValue($atributos, 'convenio.descuento', 0);
            $fecha_alta = ArrayHelper::getValue($atributos, 'convenio.fecha_alta', 0);

            $creditospagos = CreditoPagos::crearPagos(
                $id,
                $plazos,
                $total_pagar,
                $descuento,
                $fecha_alta);
        }
}