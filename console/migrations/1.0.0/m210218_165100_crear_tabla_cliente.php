<?php

use yii\db\Migration;

/**
 * Class m210209_165100_crear_tabla_cliente
 */
class m210218_165100_crear_tabla_cliente extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
         //Crear la tabla cliente
         $this->createTable('{{%cliente}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(250),
            'ap_paterno' => $this->string(250),
            'ap_materno' => $this->string(250),
            'fecha_nacimiento' => $this->date(),
            'rfc' => $this->string(13),
            'estatus' => $this->integer(2),
            'empresa_id' => $this->integer(11),
            'fecha_alta' => $this->datetime(),
            'fecha_actualizacion' => $this->datetime(),
        ], $tableOptions);

         //agregar la llave foranea a liente-
        $this->addForeignKey(
        'fk-cliente-empresa_id',
        'cliente',
        'empresa_id',
        'empresa',
        'id',
        'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210218_165100_crear_tabla_cliente cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210209_165100_crear_tabla_cliente cannot be reverted.\n";

        return false;
    }
    */
}
