<?php

use yii\db\Migration;

/**
 * Class m210209_174453_crear_tabla_credito_pagos
 */
class m210218_174453_crear_tabla_credito_pagos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%credito_pagos}}', [
            'id' => $this->primaryKey(),
            'credito_id' => $this->integer(11),
            'numero_pago' => $this->integer(11),
            'monto' => $this->decimal(10,2),
            'fecha_pago' => $this->date(),
            'estatus' => $this->integer(2),
            'fecha_alta' => $this->datetime(),
            'fecha_actualizacio' => $this->datetime(),
        ], $tableOptions);

        $this->addForeignKey(
        'fk-credito_pagos-credito_id',
        'credito_pagos',
        'credito_id',
        'credito',
        'id',
        'CASCADE'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210218_174453_crear_tabla_credito_pagos cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210209_174453_crear_tabla_credito_pagos cannot be reverted.\n";

        return false;
    }
    */
}
